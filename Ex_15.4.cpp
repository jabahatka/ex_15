
#include <iostream>

bool execution = false; //provide ability to execute program on various stage

int EvenExistanceCheckEven (int LocalNumber)  //check if there are any of even numbers
{
	if (LocalNumber < 2)
	{
		std::cout << "\nNo even numbers can be found\n";
		execution = false;
	}
	else
	{
		execution = true;
	}
	return 0;
}

int PrintEven(int LocalNumber) //print every even number under the LocalNumber (always "2" and other if any exists). Works correclty only after the EvenExistanceCheck
{
	int LocalHelper=2;

	std::cout << "2";

	while (LocalHelper <= (LocalNumber-2))
	{
		
		LocalHelper = LocalHelper + 2;

		std::cout << "\n" << LocalHelper;
	}
	return 0;
}

int PrintUneven(int LocalNumber) //print every uneven number under the LocalNumber (always "1" and other if any exists).
{
	int LocalHelper = 1;

	std::cout << "1";

	while (LocalHelper <= (LocalNumber - 2))
	{

		LocalHelper = LocalHelper + 2;

		std::cout << "\n" << LocalHelper;
	}
	return 0;
}

int main()
{
	int N;
	std::string Option;
	std::cout << "\nFor Even numbers function enter E. For Uneven munbers function enter U. To quit enter Q.\n";
	std::cin >> Option;

	if (Option == "Q")
	{
		return 2;
	}

	if (Option == "E")
	{
	while (execution == false)
	{
		std::cout << "\nSet your number or 0 to close program\n";
		std::cin >> N;

		if (N == 0)
		{
			break;
		}

		EvenExistanceCheckEven(N);
	}
	
	PrintEven(N);
	}

	if (Option == "U")
	{
		std::cout << "\nSet your number or 0 to close program\n";
		std::cin >> N;

		if (N == 0)
		{
			return 1;
		}

		PrintUneven(N);
	}

	return 0;

}
